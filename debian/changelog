members (20080128.1+nmu1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/source/format: Convert the source package to
    "3.0 (native)" format.
    This fixes lintian error malformed-debian-changelog-version.
  * Bump base version string to 20080128.1.
  * Makefile:
    + Respect environment variable CXX and CPPFLAGS
    and use them for cpp code compilation.
    - Remove the line about chown, this is completely useless.
  * debian/changelog:
    - Remove trailing unformated string.
  * debian/substvars: Removed, this should not appear in a source
    package.
  * debian/control:
    + Add Vcs-* fields to use git packaging repo under Salsa Debian
      group.
    + Bump debhelper compat to v12.
    + Bump Standards-Version to 4.4.1.
    + Use "Rules-Requires-Root: no".
  * debian/rules:
    + Use dh_auto_build to build the source code.
      This fixes FTCBFS. (Closes: #851205)
    + Rewrite with dh sequencer.
      This fixes lintian warning debian-rules-ignores-make-clean-error.

 -- Boyuan Yang <byang@debian.org>  Thu, 07 Nov 2019 17:24:12 -0500

members (20080128-5+nmu1) unstable; urgency=medium

  * Non-maintainer upload.
  * Move from the no more supported debhelper compatibility level 4 to the
    currently recommended level 9 and make debian/rules and debian/control
    more future-proof:
    + Change "dh_clean -k" to "dh_prep".
    + Add now required targets build-indep and build-arch.
    + Add ${misc:Depends} to Depends
    + Closes: #817578

 -- Axel Beckert <abe@debian.org>  Sat, 30 Jul 2016 23:54:22 +0200

members (20080128-5) unstable; urgency=low

  * #include #include <cstdio> (Closes: #504860)

 -- Lars Bahner <bahner@debian.org>  Wed, 12 Nov 2008 20:54:44 +0100

members (20080128-4) unstable; urgency=low

  * Changed `pwd` to $(CURDIR) in rules. (Closes: #462947)

 -- Lars Bahner <bahner@debian.org>  Mon, 28 Jan 2008 13:55:04 +0100

members (20080128-3) unstable; urgency=low

  * Bumped to newer version of debhelper. Cleaned packaging.

 -- Lars Bahner <bahner@debian.org>  Mon, 28 Jan 2008 12:34:47 +0100

members (20080128-2) unstable; urgency=low

  * Cleaned up text in copyright and control file

 -- Lars Bahner <bahner@debian.org>  Mon, 28 Jan 2008 12:15:33 +0100

members (20080128-1) unstable; urgency=low

  * Changed includes to be GCC-4.3 compatible. (Closes# 461684)
  * Removed useless file optparse.c from source

 -- Lars Bahner <bahner@debian.org>  Mon, 28 Jan 2008 12:04:40 +0100

members (19990831-5) unstable; urgency=low

  * Compiled with GCC 4 

 -- Lars Bahner <bahner@debian.org>  Sun, 19 Mar 2006 05:04:16 +0000

members (19990831-4) unstable; urgency=low

  * Typo with standards 3.6.1 corrected 

 -- Lars Bahner <bahner@debian.org>  Thu, 13 Nov 2003 02:11:08 +0100

members (19990831-3) unstable; urgency=low

  * Fixed dh_suidregister
  * Cleaned up in debian/rules 
  * New standards version 3.6.1
  * Compiling with -Wno-deprecated in lieu of a solution proper

 -- Lars Bahner <bahner@debian.org>  Sat,  1 Nov 2003 13:24:08 +0100

members (19990831-2.3) unstable; urgency=low

  * New maintainer (closes: #202171)
  * lintian cleanup
  * Standards 3.6.0

 -- Lars Bahner <bahner@debian.org>  Mon, 28 Jul 2003 16:57:47 +0200

members (19990831-2.2) unstable; urgency=low

  * NMU
  * Add missing include of string.h to members.cc.  (closes:
    Bug#114207)

 -- Doug Porter <dsp@debian.org>  Fri, 19 Oct 2001 13:06:24 -0500

members (19990831-2.1) unstable; urgency=low

  * NMU
  * Add missing build-depends debhelper, fix gcc 3.0 issues. Closes: #104731.

 -- LaMont Jones <lamont@debian.org>  Sat, 11 Aug 2001 17:13:32 -0600

members (19990831-2) unstable; urgency=low

  * maintainer address change.

 -- Jim Lynch <jwl@debian.org>  Mon, 10 Jan 2000 02:56:29 -0800

members (19990831-1) unstable; urgency=low

  * Fixes Bug#43719. Problem was without default, did absolutely nothing
      including check for anything or setting any exit status. Now, when
      choosing no option, a default to -a is selected.

 -- Jim Lynch <jim@laney.edu>  Tue, 31 Aug 1999 01:21:19 -0700

members (19990824-1) unstable; urgency=low

  * Added the ability to find primary and/or secondary members.

 -- Jim Lynch <jim@laney.edu>  Tue, 23 Aug 1999 08:13:00 -0700

members (19980714-1) unstable; urgency=low

  * Initial Release.

 -- Jim Lynch <jim@laney.edu>  Fri, 11 Jun 1999 15:49:02 -0700
